﻿using System;

namespace TicTacToe
{
    internal class Program
    {
        static string[,] matrix =
        {
            { "1", "2", "3" },
            { "4", "5", "6" },
            { "7", "8", "9" }
        };

        static int counter;

        static void Main(string[] args)
        {
            GamePlay();
        }

        static void GamePlay()
        {
            for (var i = 0; i < 10; i++)
            {

                if (Checker())
                {
                    Console.WriteLine("Game Ended, press any key to restart");
                    Console.ReadKey();
                    ResetMatrix();
                    i = 0;
                }
                else if (counter == 9)
                {
                    Console.WriteLine("Game Ended, press any key to restart");
                    Console.ReadKey();
                    ResetMatrix();
                    i = 0;
                }
                else
                {
                    DisplayGame();
                    while (!TryProcessUserInput())
                    {
                        Console.Clear();
                        Console.WriteLine("This place is taken");
                        DisplayGame();
                    }

                    Console.Clear();
                }
            }
        }

        static bool Checker()
        {
            // here we perform horizontal and vertical checks
            for (var i = 0; i < 3; i++)
            {
                if (matrix[i, 0] == matrix[i, 1] && matrix[i, 1] == matrix[i, 2])
                {
                    return true;
                }

                if (matrix[0, i] == matrix[1, i] && matrix[1, i] == matrix[2, i])
                {
                    return true;
                }
            }

            // here diagonal checks 
            if (matrix[0, 0] == matrix[1, 1] && matrix[1, 1] == matrix[2, 2])
            {
                return true;
            }

            if (matrix[0, 2] == matrix[1, 1] && matrix[1, 1] == matrix[2, 0])
            {
                return true;
            }

            return false;
        }

        static void ResetMatrix()
        {
            var j = 1;
            for (var i = 0; i < matrix.GetLength(0); i++)
            {

                for (var k = 0; k < matrix.GetLength(1); k++)
                {
                    var strj = j.ToString();
                    matrix[i, k] = strj;
                    j++;
                }
            }

            counter = 0;
        }

        static void DisplayGame()
        {
            var rowCount = matrix.GetLength(0);
            var columnCount = matrix.GetLength(1);
            for (var i = 0; i < rowCount; i++)
            {
                for (var k = 0; k < columnCount; k++)
                {
                    Console.Write(matrix[i, k]);
                    Console.Write(" ");
                }

                Console.WriteLine(" ");
            }
        }

        static int GetUserInput()
        {
            var input = Console.ReadLine();
            var userInput = int.Parse(input);
            return Math.Max(1, Math.Min(9, userInput));
        }

        static bool TryProcessUserInput()
        {
            var letter = counter % 2 == 0 ? "X" : "O";
            var index = GetUserInput() - 1;
            var rowIndex = index / 3;
            var columnIndex = index - 3 * rowIndex;

            var element = matrix[rowIndex, columnIndex];
            if (element.Equals("X") || element.Equals("O"))
            {
                return false;
            }

            matrix[rowIndex, columnIndex] = letter;
            ++counter;
            return true;
        }
    }
}
